package ru.anri.hiber.model;

import lombok.Value;

@Value
public class City {
	private Integer id;
	private String name;
}
