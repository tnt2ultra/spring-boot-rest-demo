package ru.anri.hiber.model.dto;

import lombok.Data;

@Data
public class CityRequest {
	private String name;
}
