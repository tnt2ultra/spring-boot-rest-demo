package ru.anri.hiber.model.dto;

import java.util.List;

import lombok.Data;

@Data
public class CountryRequest {
	private String name;
	private List<CityRequest> cities;
}
