package ru.anri.hiber.model;

import java.util.List;

import lombok.Value;

@Value
public class Country {
	private Integer id;
	private String name;
	private List<City> cities;
}
