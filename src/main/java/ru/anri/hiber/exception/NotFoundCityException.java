package ru.anri.hiber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundCityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotFoundCityException(Integer id) {
		super("City id " + id + " not found.");
	}

	public NotFoundCityException(Integer id, Throwable throwable) {
		super("City id " + id + " not found.", throwable);
	}
}
