package ru.anri.hiber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundCountryException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotFoundCountryException(Integer id) {
		super("Country id " + id + " not found.");
	}

	public NotFoundCountryException(Integer id, Throwable throwable) {
		super("Country id " + id + " not found.", throwable);
	}
}
