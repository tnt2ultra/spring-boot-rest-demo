package ru.anri.hiber.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import ru.anri.hiber.model.Country;
import ru.anri.hiber.model.dto.CountryRequest;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = CityToDtoMapper.class)
public interface CountryToDtoMapper {
	Country addCountryRequestToCountry(CountryRequest countryRequest);

	Country editCountryRequestToCountry(Integer id, CountryRequest countryRequest);
}
