package ru.anri.hiber.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import ru.anri.hiber.dao.entity.CityEntity;
import ru.anri.hiber.model.City;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CityToEntityMapper {
	CityEntity mapCityToCityEntity(City city);

	City mapCityEntityToCity(CityEntity cityEntity);

}
