package ru.anri.hiber.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import ru.anri.hiber.model.City;
import ru.anri.hiber.model.dto.CityRequest;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CityToDtoMapper {
	City addCityRequestToCity(CityRequest cityRequest);

	City editCityRequestToCity(Integer id, CityRequest cityRequest);
}
