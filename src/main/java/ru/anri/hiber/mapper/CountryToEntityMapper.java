package ru.anri.hiber.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import ru.anri.hiber.dao.entity.CountryEntity;
import ru.anri.hiber.model.Country;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = CityToEntityMapper.class)
public interface CountryToEntityMapper {
	CountryEntity mapCountryToCountryEntity(Country country);

	Country mapCountryEntityToCountry(CountryEntity countryEntity);
}
