package ru.anri.hiber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import lombok.extern.slf4j.Slf4j;

@Slf4j
//@EnableAutoConfiguration
@SpringBootApplication	// (scanBasePackages = "ru.anri.hiber")
//@ComponentScan(basePackages = { "ru.anri.hiber", "ru.anri.hiber.mapper" })
//@EntityScan("ru.anri.hiber.dao.entity")
//@EnableJpaRepositories("ru.anri.hiber.dao.repo")
public class HiberApplication {

	public static void main(String[] args) {
		log.warn("HiberApplication started");
		SpringApplication.run(HiberApplication.class, args);
	}
}
