package ru.anri.hiber.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.anri.hiber.dao.entity.CityEntity;
import ru.anri.hiber.dao.repo.CityRepo;
import ru.anri.hiber.exception.NotFoundCityException;
import ru.anri.hiber.mapper.CityToEntityMapper;
import ru.anri.hiber.model.City;

@Service
@Slf4j
@RequiredArgsConstructor
public class CityService {

	private final CityRepo cityRepo;
	private final CityToEntityMapper mapper;

	public List<City> getAllCities() {
		log.info("getAll started");
		Iterable<CityEntity> iterable = cityRepo.findAll();
		return StreamSupport.stream(iterable.spliterator(), false).map(mapper::mapCityEntityToCity)
				.collect(Collectors.toList());
	}

	public City getCityById(Integer id) {
		log.info("getCityById started {}", id);
		CityEntity cityEntity = cityRepo.findById(id).orElseThrow(() -> new NotFoundCityException(id));
		return mapper.mapCityEntityToCity(cityEntity);
	}

	public City createCity(City city) {
		log.info("createCity started {}", city);
		CityEntity cityEntity = mapper.mapCityToCityEntity(city);
		return mapper.mapCityEntityToCity(cityRepo.save(cityEntity));
	}

	public City updateCity(City city) {
		log.info("updateCity started {}", city);
		Integer id = city.getId();
		CityEntity cityEntity = cityRepo.findById(id).orElseThrow(() -> new NotFoundCityException(id));
		cityEntity.setName(city.getName());
		return mapper.mapCityEntityToCity(cityRepo.save(cityEntity));
	}

	public void deleteCity(int id) {
		log.info("deleteCity started {}", id);
		CityEntity cityEntity = cityRepo.findById(id).orElseThrow(() -> new NotFoundCityException(id));
		if (cityEntity != null) {
			cityRepo.delete(cityEntity);
		}
	}
}
