package ru.anri.hiber.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.anri.hiber.dao.entity.CityEntity;
import ru.anri.hiber.dao.entity.CountryEntity;
import ru.anri.hiber.dao.repo.CityRepo;
import ru.anri.hiber.dao.repo.CountryRepo;
import ru.anri.hiber.exception.NotFoundCountryException;
import ru.anri.hiber.mapper.CityToEntityMapper;
import ru.anri.hiber.mapper.CountryToEntityMapper;
import ru.anri.hiber.model.City;
import ru.anri.hiber.model.Country;

@Service
@Slf4j
@RequiredArgsConstructor
public class CountryService {

	private final CountryRepo countryRepo;
	private final CountryToEntityMapper mapper;
	private final CityRepo cityRepo;
	private final CityToEntityMapper mapperCity;

	public List<Country> getAllCountries() {
		log.info("getAllCountries started");
		Iterable<CountryEntity> iterable = countryRepo.findAll();
		return StreamSupport.stream(iterable.spliterator(), false).map(mapper::mapCountryEntityToCountry)
				.collect(Collectors.toList());
	}

	public Country getCountryById(Integer id) {
		log.info("getCountryById started {}", id);
		CountryEntity countryEntity = countryRepo.findById(id).orElseThrow(() -> new NotFoundCountryException(id));
		return mapper.mapCountryEntityToCountry(countryEntity);
	}

	public Country createCountry(Country country) {
		log.info("createCountry started {}", country);
		CountryEntity countryEntity = mapper.mapCountryToCountryEntity(country);
		List<CityEntity> cities = countryEntity.getCities();
		if (cities != null) {
			for (CityEntity cityEntity : cities) {
				cityEntity.setCountry(countryEntity);
			}
		}
		return mapper.mapCountryEntityToCountry(countryRepo.save(countryEntity));
	}

	public Country updateCountry(Country country) {
		log.info("updateCountry started {}", country);
		Integer id = country.getId();
		CountryEntity original = countryRepo.findById(id).orElseThrow(() -> new NotFoundCountryException(id));
		List<CityEntity> cities = original.getCities();
		if (cities != null) {
			cities.clear();
		}
		original.setName(country.getName());
		List<City> citiesCountry = country.getCities();
		if (citiesCountry != null) {
			for (City city : citiesCountry) {
				CityEntity cityEntity = mapperCity.mapCityToCityEntity(city);
				cityEntity.setCountry(original);
				CityEntity savedCityEntity = cityRepo.save(cityEntity);
				cities.add(savedCityEntity);
			}
		}
		CountryEntity savedCountryEntity = countryRepo.save(original);
		return mapper.mapCountryEntityToCountry(savedCountryEntity);
	}

	public void deleteCountry(int id) {
		log.info("deleteCountry started {}", id);
		CountryEntity countryEntity = countryRepo.findById(id).orElseThrow(() -> new NotFoundCountryException(id));
		countryRepo.delete(countryEntity);
	}
}
