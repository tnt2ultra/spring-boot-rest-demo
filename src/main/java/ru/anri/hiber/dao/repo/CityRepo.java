package ru.anri.hiber.dao.repo;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.anri.hiber.dao.entity.CityEntity;

@Repository
@Transactional
public interface CityRepo extends CrudRepository<CityEntity, Integer> {
}