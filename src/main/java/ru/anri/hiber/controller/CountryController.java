package ru.anri.hiber.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import ru.anri.hiber.mapper.CountryToDtoMapper;
import ru.anri.hiber.model.Country;
import ru.anri.hiber.model.dto.CountryRequest;
import ru.anri.hiber.service.CountryService;

@RestController
@RequestMapping(value = "/api/countries", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CountryController {

	private final CountryService countryService;
	private final CountryToDtoMapper mapper;

	@GetMapping("/")
	public List<Country> getAllCountries() {
		return countryService.getAllCountries();
	}

	@GetMapping("/{countryId}")
	public Country getCountryById(@PathVariable("countryId") int id) {
		return countryService.getCountryById(id);
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public Country createCountry(@RequestBody CountryRequest countryRequest) {
		return countryService.createCountry(mapper.addCountryRequestToCountry(countryRequest));
	}

	@PutMapping("/{countryId}")
	public Country updateCountry(@PathVariable("countryId") int id, @RequestBody CountryRequest countryRequest) {
		return countryService.updateCountry(mapper.editCountryRequestToCountry(id, countryRequest));
	}

	@DeleteMapping("/{countryId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCountry(@PathVariable("countryId") int id) {
		countryService.deleteCountry(id);
	}

}
