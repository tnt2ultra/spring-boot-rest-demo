package ru.anri.hiber.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import ru.anri.hiber.mapper.CityToDtoMapper;
import ru.anri.hiber.model.City;
import ru.anri.hiber.model.dto.CityRequest;
import ru.anri.hiber.service.CityService;

@RestController
@RequestMapping(value = "/api/cities", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CityController {

	private final CityService cityService;
	private final CityToDtoMapper mapperCity;

	@GetMapping("/")
	public List<City> getAllCities() {
		return cityService.getAllCities();
	}

	@GetMapping("/{cityId}")
	public City getCityById(@PathVariable("cityId") int id) {
		return cityService.getCityById(id);
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.CREATED)
	public City createCity(@RequestBody CityRequest cityRequest) {
		return cityService.createCity(mapperCity.addCityRequestToCity(cityRequest));
	}

	@PutMapping("/{cityId}")
	public City updateCity(@PathVariable("cityId") int id, @RequestBody CityRequest cityRequest) {
		return cityService.updateCity(mapperCity.editCityRequestToCity(id, cityRequest));
	}

	@DeleteMapping("/{cityId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCity(@PathVariable("cityId") int id) {
		cityService.deleteCity(id);
	}

}
